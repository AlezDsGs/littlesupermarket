package com.probandoboot.springbootprueba.mercadito;

public class Verduras extends Producto {
	
	private String unidadDeVenta;

	public String getUnidadDeVenta() {
		return unidadDeVenta;
	}
	public void setUnidadDeVenta(String unidadDeVenta) {
		this.unidadDeVenta = unidadDeVenta;
	}
	
	
	public Verduras(String nombreProducto, int precio, String unidadDeVenta) {
		super(nombreProducto, precio);
		this.unidadDeVenta = unidadDeVenta;
	}
	
	
public String toString() {
		
		String mensaje= "Nombre: "+getNombreProducto()+" /// "+"Precio: $"+getPrecio()+" /// "+"Unidad de venta: "+unidadDeVenta;
	
	return mensaje;
	}

}
