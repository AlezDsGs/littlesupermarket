package com.probandoboot.springbootprueba.mercadito;

public class ProgramaPrincipal {

	public static void main(String[] args) {
		
		Supermercado chino=new Supermercado("chino");
		
		chino.registrarProducto(new Bebidas("Coca-Cola Zero ",20,1.5F));
		chino.registrarProducto(new Bebidas("Coca-Cola",18,1.5F));
		chino.registrarProducto(new Limpieza("Shampoo Sedal",19,500));
		chino.registrarProducto(new Verduras("Frutillas",64,"kilo"));
		
		chino.imprimirProductos();
		
		chino.ordenamiento();
		chino.mayorMenor();
		

	}
	
	
	

}
