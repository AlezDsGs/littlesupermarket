package com.probandoboot.springbootprueba.mercadito;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Supermercado {
	
	private String nuevaEmpresa;
	private List <Producto> productos;
	
	
	
	
	
	public Supermercado(String nuevaEmpresa) {

		this.nuevaEmpresa = nuevaEmpresa;
		this.productos=new ArrayList<Producto>();
	}
	

	public String getNuevaEmpresa() {
		return nuevaEmpresa;
	}
	public void setNuevaEmpresa(String nuevaEmpresa) {
		this.nuevaEmpresa = nuevaEmpresa;
	}




	//AGREGAR PRODUCTO NUEVO
	public void registrarProducto(Producto producto){
		
		this.productos.add(producto);
		
	}
	
	
	//IMPRIMIR TODOS LOS PRODUCTOS
	public void imprimirProductos(){

		for (Producto p : productos) {
			
			System.out.println(p.toString());	
		}
		
	
		
	}
	
	public void ordenamiento() {
		
		Collections.sort(productos);
		
		
	}
	
	public void mayorMenor() {
		
		int elmayor=productos.size()-1;
		Producto mayor=(Producto)productos.get(elmayor);
		String preciomayor=mayor.getNombreProducto();
		
		
		
		Producto menor=(Producto)productos.get(0);
		String preciomenor=menor.getNombreProducto();
		
		System.out.println("=================================================");
		System.out.println("Producto mas caro: "+preciomayor);
		System.out.println("Producto mas barato: "+preciomenor);
		
	}


}
