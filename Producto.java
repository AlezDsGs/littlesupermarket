package com.probandoboot.springbootprueba.mercadito;

public class Producto implements Comparable <Producto>{
	
	private String nombreProducto;
	private int precio;
	
	//Getters y Setters
	public String getNombreProducto() {
		return nombreProducto;
	}
	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}
	
	public int getPrecio() {
		return this.precio;
	}
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	
	//constructor
	public Producto(String nombreProducto, int precio) {
		super();
		this.nombreProducto = nombreProducto;
		this.precio = precio;
	}


	public int compareTo(Producto p) {
		
		return this.precio-(p.getPrecio());
				
	}
	
	
}
