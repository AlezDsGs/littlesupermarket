package com.probandoboot.springbootprueba.mercadito;

public class Limpieza extends Producto {

	private int contenido;

	public int getContenido() {
		return contenido;
	}
	public void setContenido(int contenido) {
		this.contenido = contenido;
	}
	
	
	public Limpieza(String nombreProducto, int precio, int contenido) {
		super(nombreProducto, precio);
		this.contenido = contenido;
	}
	
public String toString() {
		
		String mensaje= "Nombre: "+getNombreProducto()+" /// "+"Contenido: "+contenido+"ml"+" /// "+"Precio: $"+getPrecio();
	
	return mensaje;
	}
	
}
