package com.probandoboot.springbootprueba.mercadito;

public class Bebidas extends Producto {
	
	private float litros;

	public float getLitros() {
		return litros;
	}
	public void setLitros(float litros) {
		this.litros = litros;
	}
	
	
	public Bebidas(String nombreProducto, int precio, float litros) {
		super(nombreProducto, precio);
		this.litros = litros;
	}
	
	
	
public String toString() {
		
		String mensaje= "Nombre: "+getNombreProducto()+" /// "+"Litros: "+litros+" /// "+"Precio: $"+getPrecio();
	
	return mensaje;
	}

}
